from dynamic_rest import routers
from students.viewsets import StudentViewSet


router = routers.DynamicRouter()
router.register('students', StudentViewSet)
