from dynamic_rest.viewsets import DynamicModelViewSet

from students.models import Student
from students.serializers import StudentSerializer


class StudentViewSet(DynamicModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
