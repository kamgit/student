from dynamic_rest.serializers import DynamicModelSerializer
from students.models import Student


class StudentSerializer(DynamicModelSerializer):
    class Meta:
        name = 'student'
        model = Student
        fields = (
            'id',
            'fio',
            'birth_date',
            'progress',
        )
        read_only_fields = (
            'id',
        )
