from django.contrib import admin

from .models import Student


class StudentAdmin(admin.ModelAdmin):
    model = Student
    fieldsets =(
        (
            '', {
                'fields': (
                    'id',
                    'fio',
                    'birth_date',
                    'progress',
                ),
            }
        ),
    )
    list_display = [
        'id',
        'fio',
        'birth_date',
        'progress',
    ]
    list_display_links = [
        'id',
        'fio',
        'birth_date',
        'progress',
    ]
    search_fields = ('fio', )
    readonly_fields=(
        'id',
    )
    list_per_page = 50


admin.site.register(Student, StudentAdmin)
