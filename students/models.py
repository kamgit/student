from django.db import models

PROGRESSES = (
    ('', 'not set'),  # не установлено
    ('unsatisfactory', 'unsatisfactory'),  # неудовлетворительно
    ('mediocre', 'mediocre'),  # удовлетворительно
    ('good', 'good',),  # хорошо
    ('excellent', 'excellent',),  # отлично
)


class Student(models.Model):
    class Meta:
        verbose_name = 'student'
        verbose_name_plural = 'students'
        unique_together = ('fio', 'birth_date')

    fio = models.CharField(
        verbose_name='firstname, surname',
        max_length=60,
        blank=True,
        default='',
    )
    birth_date = models.DateField(
        verbose_name='birth date',
    )
    progress = models.CharField(
        verbose_name='progress',
        max_length=30,
        choices=PROGRESSES,
        default=PROGRESSES[0][0],
        blank=True,
    )

    def __str__(self):
        return f'{self.fio}({self.birth_date})'
