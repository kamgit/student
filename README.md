# Student

## Project setup
```
conda create --name student python=3.8
conda activate student
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
```

## Run project
```
python manage.py runserver
```

## Comments
Administator panel:
```
http://127.0.0.1:8000/admin
```
Path for API:
```
http://127.0.0.1:8000/api
```
